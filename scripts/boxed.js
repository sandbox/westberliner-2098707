/**
 * @file
 * Provides javascript for inline editing.
 */
(function($){
  Drupal.behaviors.boxed = {
    // attach behavior once
    attach: function(context, settings) {
      $('body').once('attach-boxed', function() {
        // contextual eventhandler
        $('.boxed a').click(function(e){
          e.preventDefault();
          Drupal.behaviors.boxed.startBoxedConfiguration($(e.currentTarget).attr('href'));
        });
        // show hide placehoder
        $.each(Drupal.settings.boxed.blocksettings,function(k,v){
          var region = '.boxed-region-id-'+v;
          var blocks = $(region+' .boxed-block');
          if(blocks.length) {
            // hide placeholders
            $(region+' .boxed-placeholder').addClass('hidden');
          } else {
            $(region+' .boxed-placeholder').removeClass('hidden');
          }
        });
      });
    },
    // init boxed config
    startBoxedConfiguration: function(url) {
      Drupal.behaviors.boxed.toggleBoxedContextualLinks();
      // get form
      $.get(url+'/'+Drupal.settings.boxed.pageid+Drupal.settings.boxed.currentpage,function(data) {
        $('#'+Drupal.settings.boxed.formregion).prepend(data);
        if(!Drupal.settings.boxed.drag) {
          $('#paper-bin').hide();
        }
        $('#boxed-edit-form select').change(function(e){
          var select = $(e.currentTarget),
              val = select.val();
          Drupal.behaviors.boxed.getBlockById(val);
          select.val('');
        })
        // cancel eventhandler
        $('#boxed-edit-form input[name=cancel]').click(function(e){
          location.reload();
        });
        // save eventhandler
        $('#boxed-edit-form input[name=save]').click(function(e){
          // save
          $.post(
            Drupal.settings.boxed.saveboxedsettingsurl+'/'+Drupal.settings.boxed.pageid+Drupal.settings.boxed.currentpage,
            { token: Drupal.settings.boxed.token,
              pageid: Drupal.settings.boxed.pageid,
              theme: Drupal.settings.boxed.theme,
              blocks: Drupal.settings.boxed.pagesettings
            },
            // reload page
            function(data) {
              if(data) {
                location.reload();
              } else {
                Drupal.behaviors.boxed.boxedMessage(Drupal.t('An error occured on save.'),'error');
              }
            }
          );
        });
        // move
        if(Drupal.settings.boxed.drag) {
          var regions = $(Drupal.settings.boxed.region);
          var sort = regions.sortable({
            connectWith: regions,
            cursor: 'move',
            cursorAt: {top:5,left:5},
            dropOnEmpty: true,
            items: '.boxed-block',
            tolerance: 'pointer',
            start: Drupal.behaviors.boxed.startCallBack,
            update: Drupal.behaviors.boxed.updateCallBack,
          });
        }
        // if placebar is in use attach clickhandler
        if(Drupal.settings.boxed.placebar) {
          $('.boxed-img').click(Drupal.behaviors.boxed.placebarHandler);
        }
        // add dashed
        $(Drupal.settings.boxed.region).addClass('boxed-dashed');
        $(Drupal.settings.boxed.region +' > div.contextual-links-wrapper').hide();
      });
    },
    // hide contextual region links in boxed edit mode
    toggleBoxedContextualLinks: function() {
      $.each($('.boxed').closest('.contextual-links-region'),function(k,v) {
        var dom = $(v);
        (dom.hasClass('boxed-edit-region'))?dom.removeClass('boxed-edit-region'):dom.addClass('boxed-edit-region');
      })
    },
    // load block by blockid
    getBlockById: function(blockid) {
      var url = Drupal.settings.boxed.getblockurl+'/'+blockid+'/'+Drupal.settings.boxed.pageid+Drupal.settings.boxed.currentpage;
      $.ajax({
        url: url,
        success: function(data) {
          Drupal.behaviors.boxed.addBlock(data,blockid);
        },
        error: function(data) {
          console.log(data);
        },
        dataType: 'html'
      });
    },
    // add block to first region
    addBlock: function(data,blockid) {
      var id = $(data).attr('id');
      if(id == '') {
        Drupal.behaviors.boxed.boxedMessage(Drupal.t('An error occured by getting this block.'),'error');
        return false;
      }
      $(data).addClass('boxed-block');
      // check if block rendered probably
      if($('#'+id).length == 0) {
        region = $(Drupal.settings.boxed.firstregion);
        if(region.length == 0) {
          region = $(Drupal.settings.boxed.region);
        }
        region.first().append(data);
        var block = $('#'+id);
        // adding blockid
        block.addClass('boxed-block').attr('rel',blockid);
        // adding dragging functionality
        if(Drupal.settings.boxed.drag) {
          block.addClass('drag')
        }
        // adding placebar functionality
        if(Drupal.settings.boxed.placebar) {
          block.addClass('boxed-placebar')
          block.find('.boxed-img').click(Drupal.behaviors.boxed.placebarHandler);
        }
        // adding behaviors such as contextual
        Drupal.attachBehaviors(block);
        // send message
        Drupal.behaviors.boxed.boxedMessage(Drupal.t('Block added.'),'status');
        // update boxed settings array
        Drupal.behaviors.boxed.boxedUpdate();
      } else {
        Drupal.behaviors.boxed.boxedMessage(Drupal.t('Block already added.'),'error');
      }
    },
    // message wrapper function
    boxedMessage: function(msg,status) {
      var msgdom = $('#boxed-message');
      msgdom.stop(true,true).hide();
      msgdom.html('');
      msgdom.attr('class','');

      msgdom.addClass(status).html(msg);
      msgdom.slideDown('slow').delay(4000).slideUp('slow');
    },
    // updates boxed settings array
    boxedUpdate: function() {
      $('#paper-bin .boxed-block').remove();
      Drupal.settings.boxed.pagesettings = {};
      $.each(Drupal.settings.boxed.blocksettings,function(k,v){
        var region = '.boxed-region-id-'+v;
        var blocks = $(region+' .boxed-block');
        if(blocks.length) {
          // hide placeholders
          $(region+' .boxed-placeholder').addClass('hidden');
          var blocklist = [];
          $.each(blocks,function(key,block){
            var blockid = $(block).attr('rel').split('/');
            blocklist.push({'module':blockid[0],'delta':blockid[1]});
          })
          Drupal.settings.boxed.pagesettings[k] = blocklist;
        } else {
          $(region+' .boxed-placeholder').removeClass('hidden');
        }
      });
    },
    // draggable eventhandling
    startCallBack: function (event, ui) {
      $(ui.item).css({'width':'200px','height':'100px','overflow':'hidden'});
    },
    overCallBack: function (event, ui) {
      //console.log(event,ui);
    },
    sortCallBack: function (event, ui) {
      //console.log(event,ui);
    },
    updateCallBack: function (event, ui) {
      // update array
      Drupal.behaviors.boxed.boxedUpdate();
    },
    // placebar eventhandling
    placebarHandler: function (e) {
      var btn = $(e.currentTarget),
          type = btn.attr('alt'),
          block = btn.closest('.boxed-block');

      if(type == 'kill') {
        block.remove();
      }
      if(type == 'up') {
        Drupal.behaviors.boxed.up(block);
      }
      if(type == 'down') {
        Drupal.behaviors.boxed.down(block);
      }
      Drupal.behaviors.boxed.boxedUpdate();
    },
    // up logic for placebar
    up: function(block) {
      blockid = block.attr('rel').split('/');
      // get position within boxed settings array cause based on rendered html is not reliable
      blockPosition = Drupal.behaviors.boxed.getBlockIndex(blockid[0], blockid[1]);
      settings = Drupal.settings.boxed.pagesettings;
      if(!blockPosition) {
        return false;
      }
      // switch region or switch position within region
      if(blockPosition.index == 0) {
        Drupal.behaviors.boxed.switchRegion(block,blockPosition,'up');
      } else {
        Drupal.behaviors.boxed.moveBlockWithinRegion(block,blockPosition,'up');
      }
    },
    // down logic for placebar
    down: function(block) {
      blockid = block.attr('rel').split('/');
      // get position within boxed settings array cause based on rendered html is not reliable
      blockPosition = Drupal.behaviors.boxed.getBlockIndex(blockid[0], blockid[1]);
      settings = Drupal.settings.boxed.pagesettings;
      if(!blockPosition) {
        return false;
      }
      // switch region or switch position within region
      if(blockPosition.index+1 == settings[blockPosition.region].length) {
        Drupal.behaviors.boxed.switchRegion(block,blockPosition,'down');
      } else {
        Drupal.behaviors.boxed.moveBlockWithinRegion(block,blockPosition,'down');
      }
    },
    // get position within boxed settings array cause based on rendered html is not reliable
    getBlockIndex: function(module,delta) {
      var blockPosition = false;
      $.each(Drupal.settings.boxed.pagesettings, function(region,blocks){
        $.each(blocks, function(k,block){
          if(block.module == module && block.delta == delta) {
            blockPosition = {region: region, index: k};
            return false;
          };
        });
      });
      return blockPosition;
    },
    // switch region
    switchRegion: function(block,blockPosition,direction) {
      // get regions
      var regions = $(Drupal.settings.boxed.region+':[id!=paper-bin]'),
          region = block.closest(Drupal.settings.boxed.region),
          otherRegionIndex = Drupal.behaviors.boxed.getOtherRegionIndex(region,direction),
          otherRegion = $(regions[otherRegionIndex]);
      // up is the easy one
      if(direction == 'up') {
        otherRegion.append(block);
      }
      // down is depending witch block is already in otherRegion
      if(direction == 'down') {
        if(otherRegion.children('.boxed-block').length) {
          block.insertBefore(otherRegion.children('.boxed-block').first());
        }
        if(otherRegion.children('.block:not(.boxed-block)').length && Drupal.settings.boxed.fixed) {
          block.insertAfter(otherRegion.children('.block:not(.boxed-block)').last());
        } else {
          otherRegion.prepend(block);
        }
      }
    },
    // function tells it all
    moveBlockWithinRegion: function(block,blockPosition,direction) {
      region = block.closest(Drupal.settings.boxed.region);
      blocks = region.find('.boxed-block');
      if(direction == 'up') {
        block.insertBefore(blocks[blockPosition.index-1]);
      }
      if(direction == 'down') {
        block.insertAfter(blocks[blockPosition.index+1]);
      }
    },
    // get other region index depending by direction
    getOtherRegionIndex: function(region,direction) {
      var regions = $(Drupal.settings.boxed.region+':[id!=paper-bin]'),
          returnRegionIndex = false,
          regionIndex = 0,
          dir = (direction == 'up')?-1:1;
      $.each(regions, function(k,otherRegion) {
        if(otherRegion == region[0]) {
          regionIndex = k;
          return false;
        }
      });

      returnRegionIndex = regionIndex+dir;

      if(regionIndex == 0 && direction == 'up') {
        returnRegionIndex = regions.length-1;
      }
      if(regionIndex == regions.length-1 && direction == 'down') {
        returnRegionIndex = 0;
      }
      return returnRegionIndex;
    }
  }
})(jQuery)
