CONTENTS OF THIS FILE
---------------------
 * Author
 * Description
 * Installation
 * Usage

AUTHOR
------
Patrick Herzberg ("westberliner", https://drupal.org/user/1600566)

DESCRIPTION
-----------
This module allows to rearange blocks on every page specified by the boxed settings.
It should be seen as an addition to the block module.

INSTALLATION
------------
1. Download UUID Module.
2. Download Boxed Module and activate it.

USAGE
-----
1. Under admin/config/user-interface/boxed configurate the module to your needs.
2. Via contextual link edit page.
