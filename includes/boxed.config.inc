<?php
/**
 * @file
 * Provides ajax form for inline block editing.
 */
/**
 * page callback boxed_configuration
 */
function boxed_edit_blocks_form($form, $form_state) {
  $blocks = boxed_get_available_blocks();
  $form = array();
  $form['boxed'] = array(
    '#type' => 'fieldset',
    '#title' => 'Boxed',
    '#prefix' => '<div id="boxed-edit-form"><div class="section">',
    '#suffix' => '</div></div>',
  );
  $form['boxed']['select'] = array(
    '#type' => 'select',
    '#title' => t('Available blocks'),
    '#options' => $blocks
  );
  $form['boxed']['paperbin'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="paper-bin" class="boxed-edit-region boxed-dashed"></div>'
  );
  $form['boxed']['messages'] = array(
    '#type' => 'markup',
    '#markup' => '<p id="boxed-message"></p>'
  );
  $form['boxed']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('cancel'),
    '#name' => 'cancel',
  );
  $form['boxed']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('save'),
    '#name' => 'save',
  );
  $form = drupal_render($form);
  drupal_json_output($form);
  drupal_exit();
}
