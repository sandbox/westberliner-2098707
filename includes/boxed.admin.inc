<?php
/**
 * @file
 * Provides admin settings.
 */

/**
 * page callback admin settings for this module
 */
function boxed_admin_settings_form($form, $form_state) {
  $theme_key = variable_get('theme_default', NULL);
  $themes = array_keys(array_filter(list_themes(), '_boxed_status'));
  $node_types = array_keys(node_type_get_types());

  $settings = variable_get('boxed_settings', array());

  $form = array();
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => array_combine($themes, $themes),
    '#default_value' => $theme_key,
    '#description' => t('Select Theme to apply boxed.'),
  );
  foreach ($themes as $theme) {
    $regions = array_keys(system_region_list($theme));
    $form[$theme] = array(
      '#type' => 'fieldset',
      '#title' => t('Regions of %s', array('%s' => $theme)),
      '#states' => array(
        'visible' => array(
          ':input[name="theme"]' => array('value' => $theme),
        ),
      ),
      '#tree' => TRUE,
    );
    $form[$theme]['blocksettings'] = array(
      '#type' => 'select',
      '#options' => array_combine($regions, $regions),
      '#default_value' => (isset($settings[$theme]['blocksettings']))?$settings[$theme]['blocksettings']:array(),
      '#multiple' => TRUE,
      '#description' => t('Select possible areas to apply boxed.'),
      '#ajax' => array(
        'event' => 'change',
        'callback' => 'boxed_admin_settings_js_callback',
        'wrapper' => 'firstregion',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
    $form[$theme]['drag'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable dragging'),
      '#options' => array(TRUE => t('Enabled'), FALSE => t('Disabled')),
      '#default_value' => (isset($settings[$theme]['drag']))?$settings[$theme]['drag']:TRUE,
      '#description' => t('If your theme breaks the jQuery UI draggable feature, disable it.'),
    );
    $form[$theme]['placebar'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Placebar'),
      '#options' => array(TRUE => t('Enabled'), FALSE => t('Disabled')),
      '#default_value' => (isset($settings[$theme]['placebar']))?$settings[$theme]['placebar']:FALSE,
      '#description' => t('If dragging is disabled or you need a alternativ for moving blocks around.'),
    );
    $form[$theme]['fixed_block'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Fixed Block'),
      '#options' => array(TRUE => t('Enabled'), FALSE => t('Disabled')),
      '#default_value' => (isset($settings[$theme]['fixed_block']))?$settings[$theme]['fixed_block']:TRUE,
      '#description' => t('If you added blocks via block module to a boxed region, decide if boxed float around or set to fixed to ignore them.'),
    );
    $form[$theme]['formregion'] = array(
      '#type' => 'textfield',
      '#title' => t('Region for Boxedform'),
      '#description' => t('Set a css id value (without #) to prepend form to.'),
      '#default_value' => (isset($settings[$theme]['formregion']))?$settings[$theme]['formregion']:'',
    );
    $form[$theme]['inner'] = array(
      '#type' => 'textfield',
      '#title' => t('Inner Region'),
      '#description' => t('Set a css class value (without .) if there is any inner dom in your regions.'),
      '#default_value' => (isset($settings[$theme]['inner']))?$settings[$theme]['inner']:'',
    );
    $form[$theme]['firstregion'] = array(
      '#type' => 'select',
      '#title' => t('First Region'),
      '#description' => t('Select the Region where the Block will be applied at first.'),
      '#default_value' => (isset($settings[$theme]['firstregion']))?$settings[$theme]['firstregion']:'',
      '#options' => (isset($settings[$theme]['blocksettings']))?$settings[$theme]['blocksettings']:array(),
      '#prefix' => '<div id="firstregion">',
      '#suffix' => '</div>',
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('save'),
    '#submit' => array('boxed_admin_settings_form_submit'),
  );

  form_load_include($form_state, 'inc', 'boxed', 'includes/boxed.admin');

  return $form;
}

function boxed_admin_settings_js_callback($form, $form_state) {
  $theme = $form_state['values']['theme'];
  $values = array_filter($form_state['values'][$theme]['blocksettings']);
  $form[$theme]['firstregion']['#options'] = $values;
  return $form[$theme]['firstregion'];
}

function boxed_admin_settings_form_submit($form, $form_state) {
  $node_types = array_keys(node_type_get_types());
  $values = $form_state['values'];

  $settings = array();

  $themes = array_keys(array_filter(list_themes(), '_boxed_status'));
  foreach ($themes as $theme) {
    if (isset($values[$theme])) {
      $settings[$theme] = array(
        'blocksettings' => array_filter($values[$theme]['blocksettings']),
        'formregion' => check_plain($values[$theme]['formregion']),
        'inner' => check_plain($values[$theme]['inner']),
        'drag' => $values[$theme]['drag'],
        'fixed_block' => $values[$theme]['fixed_block'],
        'placebar' => $values[$theme]['placebar'],
        'firstregion' => (!empty($values[$theme]['firstregion']))?$values[$theme]['firstregion']:'',
      );
    }
  }
  variable_set('boxed_settings', $settings);
  drupal_set_message(t('Boxed settings successfully saved.'));
}

function _boxed_status($theme) {
  return $theme->status;
}
